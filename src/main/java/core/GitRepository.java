package core;

import java.io.BufferedReader;
import java.io.FileReader;

public class GitRepository {

	private String repoPath;
	
	public GitRepository(String repoPath){
		
		this.repoPath = repoPath;
	}
	
	public String getHeadRef() {
		
		String headRef = "Head not found";
		try {
			BufferedReader br = new BufferedReader(new FileReader(repoPath+"/HEAD"));
			headRef = br.readLine().substring(5);
			br.close();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		return headRef;
	}
	
	public String getRefHash(String fileName) {
		
		String refHash = "refHash not found";
		try {
			BufferedReader br = new BufferedReader(new FileReader(repoPath +"/"+ fileName));
			refHash = br.readLine();
			br.close();
			return refHash;
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		return refHash;
	}

}
	